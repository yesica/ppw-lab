from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .custom_auth import auth_login, auth_logout
from .views import index
from .models import Pengguna, MovieKu
import environ

# Create your tests here.
root = environ.Path(__file__) -3
env = environ.Env(DEBUG=(bool, False),)
environ.Env.read_env('.env')

class Lab10UnitTest(TestCase):
	def setUp(self):
		self.username = env("SSO_USERNAME")
		self.password = env("SSO_PASSWORD")
		self.movieId = 'tt1653823'

	def test_lab_10_url_is_exist(self):
		response = Client().get('/lab-10/')
		self.assertEqual(response.status_code, 200)

	def test_login_failed(self):
		response = self.client.post('/lab-10/custom_auth/login/', {'username': "", 'password': ""})
		html_response = self.client.get('/lab-10/').content.decode('utf-8')

	def test_lab_10_page_when_user_is_logged_in_or_not(self):
		#not logged in, render login template
		response = self.client.get('/lab-10/')
		self.assertEqual(response.status_code, 200)
		self.assertTemplateUsed('lab_10/session/login.html')

		#logged in, redirect to dashboard page
		response = self.client.post('/lab-10/custom_auth/login/', {'username': self.username, 'password': self.password})
		self.assertEqual(response.status_code, 302)
		response = self.client.get('/lab-10/')
		self.assertEqual(response.status_code, 302)
		self.assertTemplateUsed('lab_10/session/dashboard.html')

	def test_direct_access_to_dashboard_url(self):
		#not logged in, redirect to login page
		response = self.client.get('/lab-10/dashboard/')
		self.assertEqual(response.status_code, 302)

		#logged in, render dashboard template
		response = self.client.post('/lab-10/custom_auth/login/', {'username': self.username, 'password': self.password})
		self.assertEqual(response.status_code, 302)
		response = self.client.get('/lab-10/dashboard/')
		self.assertEqual(response.status_code, 200)

	def test_access_dashboard_login(self):
		response = self.client.post('/lab-10/custom_auth/login/',{'username':self.username, 'password': self.password})
		self.assertEqual(response.status_code, 302)
		response = self.client.get('/lab-10/dashboard/')
		self.assertEqual(response.status_code, 200)
		self.assertEqual(Pengguna.objects.all().count(),1)

	def test_movie_list(self):
		response = self.client.post('/lab-10/custom_auth/login/',{'username':self.username, 'password': self.password})
		self.assertEqual(response.status_code, 302)

		response = self.client.get('/lab-10/movie/list/')
		self.assertEqual(response.status_code, 200)

	def test_movie_detail(self):
		response = self.client.get('/lab-10/movie/detail/'+self.movieId+'/')
		self.assertEqual(response.status_code, 200)

		response = self.client.post('/lab-10/custom_auth/login/',{'username':self.username, 'password': self.password})
		self.assertEqual(response.status_code, 302)
		response = self.client.get('/lab-10/dashboard/')
		self.assertEqual(response.status_code, 200)
		response = self.client.get('/lab-10/movie/detail/'+self.movieId+'/')
		self.assertEqual(response.status_code, 200)

	def test_add_watch_later_logged_in(self):
		response = self.client.post('/lab-10/custom_auth/login/',{'username':self.username, 'password': self.password})
		self.assertEqual(response.status_code, 302)
		response = self.client.get('/lab-10/dashboard/')
		self.assertEqual(response.status_code, 200)
		response = self.client.get('/lab-10/movie/detail/'+self.movieId+'/')
		self.assertEqual(response.status_code, 200)
		response = self.client.get('/lab-10/movie/watch_later/add/'+self.movieId+'/')
		self.assertEqual(response.status_code, 302)
		response = self.client.get('/lab-10/movie/watch_later/add/'+self.movieId+'/')
		self.assertEqual(response.status_code, 302)

	def test_add_watch_later_without_logged_in(self):
		response = self.client.get('/lab-10/movie/watch_later/add/'+self.movieId+'/')
		self.assertEqual(response.status_code, 302)
		response = self.client.get('/lab-10/movie/watch_later/add/'+self.movieId+'/')
		self.assertEqual(response.status_code, 302)

	def test_list_watch_later(self):
		response = self.client.get('/lab-10/movie/watch_later/')
		self.assertEqual(response.status_code, 200)
		response = self.client.post('/lab-10/custom_auth/login/',{'username':self.username, 'password': self.password})
		self.assertEqual(response.status_code, 302)
		response = self.client.get('/lab-10/dashboard/')
		self.assertEqual(response.status_code, 200)
		response = self.client.get('/lab-10/movie/watch_later/add/'+self.movieId+'/')
		self.assertEqual(response.status_code, 302)
		response = self.client.get('/lab-10/movie/watch_later/')
		self.assertEqual(response.status_code, 200)

	def test_set_watched(self):
		response = self.client.post('/lab-10/custom_auth/login/',{'username':self.username, 'password': self.password})
		self.assertEqual(response.status_code, 302)
		response = self.client.get('/lab-10/dashboard/')
		self.assertEqual(response.status_code, 200)
		response = self.client.get('/lab-10/movie/watch_later/add/'+self.movieId+'/')
		self.assertEqual(response.status_code, 302)
		response = self.client.get('/lab-10/movie/watched/'+self.movieId+'/')
		self.assertEqual(response.status_code, 302)

	def test_list_watched(self):
		response = self.client.post('/lab-10/custom_auth/login/',{'username':self.username, 'password': self.password})
		self.assertEqual(response.status_code, 302)
		response = self.client.get('/lab-10/dashboard/')
		self.assertEqual(response.status_code, 200)
		response = self.client.get('/lab-10/movie/watch_later/add/'+self.movieId+'/')
		self.assertEqual(response.status_code, 302)
		response = self.client.get('/lab-10/movie/watched/'+self.movieId+'/')
		self.assertEqual(response.status_code, 302)
		response = self.client.get('/lab-10/movie/watched/')
		self.assertEqual(response.status_code, 200)

	def test_logout(self):
		response = self.client.post('/lab-10/custom_auth/login/', {'username': self.username, 'password': self.password})
		self.assertEqual(response.status_code, 302)
		response = self.client.post('/lab-10/custom_auth/logout/')
		html_response = self.client.get('/lab-10/').content.decode('utf-8')
		self.assertEqual(response.status_code, 302)

	#def test_api(self):
	#	response = self.client.get('/lab-10/api/movie/see/2000/')
	#	self.assertEqual(response.status_code, 200)
	#	response = self.client.get('/lab-10/api/movie/-/-/')
	#	self.assertEqual(response.status_code, 200)

	def test_pengguna_model(self):
		Pengguna.objects.create(kode_identitas='1606830676',nama="yesica")
		self.assertEqual(Pengguna.objects.all().count(),1)

	def test_movieku_model(self):
		pengguna = Pengguna.objects.create(kode_identitas='1606830676',nama="yesica")
		MovieKu.objects.create(pengguna=pengguna, kode_movie = self.movieId)
		self.assertEqual(MovieKu.objects.all().count(), 1)
